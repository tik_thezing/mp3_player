/**
  keycardReader

  Reads and reacts to RFID cards
  Based on Arduino Examples MFRC522 ReadAndWrite
  
  circuit:
  - Arduino RFID Card Reader MFRC522 on pins D9, D10

  created 24 May 2021 
  by Chris Bollermann
 */

#include <SPI.h>      // Serial Peripheral Interface (SPI)
#include <MFRC522.h>  // RFID Card Reader RC522

#define RST_PIN         9           // Configurable, see circuit
#define SS_PIN          10          // Configurable, see circuit

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;

/**
 * Initialize.
 */
 
void setup() {
    Serial.begin(9600); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card

    // Prepare the key (used both as key A and as key B)
    // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    // Header for the keycardReader interface
    Serial.println(F("Show any card:"));  
}

/**
 * Main loop.
 */
 
void loop() {
   
    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;

    // In this sketch we use the second sector,
    // that is: sector #1, covering block #4 up to and including block #7
    byte sector         = 1;
    byte blockAddr      = 4;
    byte trailerBlock   = 7;
    MFRC522::StatusCode status;
    byte buffer[18];
    byte size = sizeof(buffer);

    // Authenticate using key A
    //Serial.println(F("Authenticating using key A..."));
    status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    // Read data from the card
    mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    byte colourCode = buffer[0];
   
   // BEGINNING OF TASK 

   // 
   if (colourCode == 0x01) { // RED equals 01 (0x01)
    Serial.println(F("Card Type: RED"));
    Serial.println(F("Access denied!"));
   }

    else if (colourCode == 0x02) { // BLUE equals 02 (0x02) 
    Serial.println(F("Card Type: BLUE"));
    Serial.println(F("Access denied!"));
    }

    else if (colourCode == 0x03) { // GREEN equals 03 (0x03) 
    Serial.println(F("Card Type: GREEN"));
    Serial.println(F("Access denied!"));
    }

    else {
    Serial.println(F("Card Type: UNKNOWN"));
    Serial.println(F("Access denied!"));
    }

    // END OF TASK

    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();

    // Wait for another card
    Serial.println();
    Serial.println(F("Show any card:"));    
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
