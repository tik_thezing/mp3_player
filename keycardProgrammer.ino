/*
 * Code colour into RFID card
 * Based on Arduino Examples MFRC522
 * 
 * circuit:
 * - Arduino RFID Card Reader MFRC522 on pins D9, D10
 * 
 * created 11 Mar 2022
 * by Tik Tipayanond
 */
#include <SPI.h>      // Serial Peripheral Interface (SPI)
#include <MFRC522.h>  // RFID Card Reader RC522

#define RST_PIN         9           // Configurable, see circuit
#define SS_PIN          10          // Configurable, see circuit

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;

// on card colour
const byte COLOUR_CODE_RED = 0x01;
const byte COLOUR_CODE_BLUE = 0x02;
const byte COLOUR_CODE_GREEN = 0x03;
byte currColourCode = COLOUR_CODE_RED;

/**
 * Initialize.
 */
 
void setup() {
    Serial.begin(9600); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card
    if(mfrc522.PCD_PerformSelfTest())
      Serial.println("Passed Self-Test");
      
    // Prepare the key (used both as key A and as key B)
    // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }

    dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
    // Header for the keycardReader interface
    Serial.println(F("Programming this card ...")); 
}

void loop() {
    
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;

    if ( ! mfrc522.PICC_ReadCardSerial())
        return;


    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    // Check for compatibility
    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("This sample only works with MIFARE Classic cards."));
        return;
    }
  
  // In this sketch we use the second sector,
  // that is: sector #1, covering block #4 up to and including block #7
  byte sector         = 1;
  byte blockAddr      = 4;
  byte dataBlock[]    = {
        0x01, 0x02, 0x03, 0x04, //  1,  2,   3,  4,
        0x05, 0x06, 0x07, 0x08, //  5,  6,   7,  8,
        0x09, 0x0a, 0xff, 0x0b, //  9, 10, 255, 11,
        0x0c, 0x0d, 0x0e, 0x0f  // 12, 13, 14, 15
  };
  byte trailerBlock   = 7;
  MFRC522::StatusCode status;
  byte buffer[18];
  byte size = sizeof(buffer);

  // Authenticate using key B
  //Serial.println(F("Authenticating using key A..."));
  status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("PCD_Authenticate() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  // Show the whole sector as it currently is
  Serial.println(F("Current data in sector:"));
  mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
  Serial.println();
    
  switch(currColourCode) {
    case COLOUR_CODE_RED: Serial.println(F("programming card to RED"));  
               dataBlock[0] = COLOUR_CODE_RED;       
               break;

    case COLOUR_CODE_BLUE: Serial.println(F("programming card to BLUE"));
               dataBlock[0] = COLOUR_CODE_BLUE;
               break;

    case COLOUR_CODE_GREEN: //fall through
    default:   Serial.println(F("programming card to GREEN"));
               dataBlock[0] = COLOUR_CODE_GREEN;
               break;
  }
  dump_byte_array(dataBlock, 16); Serial.println();
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        Serial.print(F("Exiting on error"));
        return;
    }else {
      Serial.println("Done");
    }
    Serial.println("");
    // END OF TASK
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();

    //Wait for another card
    currColourCode += 1;
    if(currColourCode > COLOUR_CODE_GREEN) {
      currColourCode = COLOUR_CODE_RED;
    }
    Serial.println();
    Serial.println("Programming this card...");
    delay(1000);
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}
